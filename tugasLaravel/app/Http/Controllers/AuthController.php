<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('signup.formSignUp');
    }

    public function selamatDatang(Request $request)
    {
       $namadepan = $request['namadepan'];
       $namabelakang = $request['namabelakang'];

       return view("signup.welcome", ['namadepan' => $namadepan , 'namabelakang' => $namabelakang]);
    }
};
