<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Buat Account Baru</title>
</head>
<body>

    <h2>Sign Up Form</h2>

    <form action="/welcome" method = "post">
    @csrf   
        <label for="First Name">First Name:</label><br>
        <input type="text" name="namadepan" id="namadepan"><br><br>
        <label for="Last Name">Last Name:</label><br>
        <input type="text" name="namabelakang" id="namabelakang"><br><br>
        <label for="Gender">Gender:</label><br>
        <input type="radio" name="statu">Male<br>
        <input type="radio" name="status">Female<br>
        <input type="radio" name="status">Other<br><br>
        <label for="Nationality">Nationality:</label><br>
        <select name="Nationality">
            <option value="Indonesian" name="status">Indonesian</option>
            <option value="English" name="status">English</option>
            <option value="Other" name="status">Other</option>
        </select><br><br>
        <label for="Language Spoken"> Language Spoken:</label><br><br>
        <input type="checkbox" name="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="English"> English <br>
        <input type="checkbox" name="Other"> Other <br>
        <label for="Bio">Bio:</label> <br>
        <textarea name="massage" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">

</body>
</html>
